import SectionCreator from "./join-us-section.js";
import validate from "./email-validator.js";
import "./styles/style.css";

document.addEventListener("DOMContentLoaded", () => {
  const sectionCreator = new SectionCreator();
  const realSection = sectionCreator.create("standard");

  const mainContainer = document.getElementById("app-container");
  const footer = document.querySelector("footer");

  mainContainer.insertBefore(realSection, footer);

  const form = realSection.querySelector("form");
  const emailInput = realSection.querySelector(".email-input");
  const subscribeButton = realSection.querySelector(".subscribe-button");

  function hideEmailInput() {
    emailInput.style.display = "none";
    subscribeButton.textContent = "Unsubscribe";
  }

  function showEmailInput() {
    emailInput.style.display = "block";
    subscribeButton.textContent = "Subscribe";
  }

  form.addEventListener("submit", (e) => {
    e.preventDefault();
    const emailValue = emailInput.value;

    const isValidEmail = validate(emailValue);
    if (isValidEmail) {
      hideEmailInput();
      localStorage.setItem("email", emailValue);
    }

    emailInput.value = "";
  });

  const storedEmail = localStorage.getItem("email");
  if (storedEmail) {
    emailInput.value = storedEmail;
  }

  subscribeButton.addEventListener("click", () => {
    if (emailInput.style.display === "none") {
      showEmailInput();
      localStorage.removeItem("email");
    } else {
      hideEmailInput();
    }
  });
});
